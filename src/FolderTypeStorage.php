<?php

namespace Drupal\folder;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Defines a storage handler class for folder types.
 */
class FolderTypeStorage extends ConfigEntityStorage implements FolderTypeStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getToplevelFids(array $folder_type_ids, int $uid = 0): array {
    $query = \Drupal::entityQuery('folder')
      ->accessCheck(TRUE)
      ->condition('type', $folder_type_ids, 'IN')
      ->condition('parent.target_id', 0);
    if ($uid) {
      $query->condition('uid', $uid);
    }
    $fids = $query->execute();

    return $fids ? array_values($fids) : [];
  }

  public function getToplevelTids($vids) {
    return $this->getToplevelFids($vids);
  }

}
