<?php

namespace Drupal\folder\Plugin\Validation\Constraint;

use Drupal\taxonomy\Plugin\Validation\Constraint\TaxonomyTermHierarchyConstraint;

/**
 * Validation constraint for changing the folder hierarchy in pending revisions.
 *
 * @Constraint(
 *   id = "FolderHierarchy",
 *   label = @Translation("Folder hierarchy.", context = "Validation"),
 * )
 */
class FolderHierarchyConstraint extends TaxonomyTermHierarchyConstraint {

  /**
   * The default violation message.
   *
   * @var string
   */
  public $message = 'You can only change the hierarchy for the <em>published</em> version of this folder.';

}
