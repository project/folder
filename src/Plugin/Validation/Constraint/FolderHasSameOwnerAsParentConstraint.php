<?php

namespace Drupal\folder\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a folder has the same owner as its parent.
 *
 * @Constraint(
 *   id = "FolderHasSameOwnerAsParent",
 *   label = @Translation("Foder has same user as parent constraint", context = "Validation"),
 * )
 */
class FolderHasSameOwnerAsParentConstraint extends Constraint {

  public $message = '@folder is required to have @parent_user as its owner and not @user because it has @parent as parent folder.';

}
