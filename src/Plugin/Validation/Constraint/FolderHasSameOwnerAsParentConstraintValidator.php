<?php

namespace Drupal\folder\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the FolderHasSameOwnerAsParent constraint.
 */
class FolderHasSameOwnerAsParentConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    if (!$item = $value->first()) {
      return;
    }

    /** @var \Drupal\folder\Entity\FolderInterface $folder */
    $folder = $value->getEntity();
    if (!($parent = $folder->getParent())) {
      return;
    }

    $parent_owner = $parent->getOwner();
    $owner = $folder->getOwner();
    if ($parent_owner->id() !== $owner->id()) {
      $this->context->addViolation($constraint->message, [
        '@folder' => $folder->getName(),
        '@parent_owner' => $parent_owner->getDisplayName(),
        '@parent' => $parent->getName(),
        '@user' => $owner->getDisplayName(),
      ]);
    }
  }

}
