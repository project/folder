<?php

namespace Drupal\folder\Plugin\Validation\Constraint;

use Drupal\taxonomy\Plugin\Validation\Constraint\TaxonomyTermHierarchyConstraintValidator;

/**
 * Constraint validator for changing folder parents in pending revisions.
 */
class FolderHierarchyConstraintValidator extends TaxonomyTermHierarchyConstraintValidator {
}
