<?php

namespace Drupal\folder\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Plugin\Validation\Constraint\CompositeConstraintBase;

/**
 * Validation constraint for changing the folder hierarchy in pending revisions.
 *
 * @Constraint(
 *   id = "FolderName",
 *   label = @Translation("Folder name.", context = "Validation"),
 * )
 */
class FolderNameConstraint extends CompositeConstraintBase {

  public string $message = 'There is an existing folder with %name as name in %parent parent folder.';

  /**
   * {@inheritdoc}
   */
  public function coversFields() {
    return ['name', 'parent'];
  }

}
