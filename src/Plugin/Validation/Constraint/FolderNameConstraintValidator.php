<?php

namespace Drupal\folder\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\folder\FolderStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the folder name constraint.
 *
 * This class is responsible for validating the uniqueness of folder names within a parent folder.
 * It checks if the given folder name already exists among the children of the parent folder.
 * If a duplicate name is found, a violation is added to the validation context.
 */
class FolderNameConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Folder storage handler.
   *
   * @var \Drupal\folder\FolderStorage
   */
  protected FolderStorage $folderStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static($container);
    $instance->folderStorage = $container->get('entity_type.manager')->getStorage('folder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    /** @var \Drupal\folder\Entity\FolderInterface $value */

    $name = $value->name->value;
    $parent = $value->getParent();
    $owner = $value->getOwner();
    $children = $this->folderStorage->getChildren($parent, $owner->id());
    foreach ($children as $child) {
      if ($child->getName() === $name && $child->id() !== $value->id()) {
        $params = [
          '%name' => $name,
          '%parent' => $parent ? $parent->getName() : (string)t('<root>'),
        ];
        $this->context->buildViolation($constraint->message, $params)
          ->atPath('name')
          ->addViolation();
        break;
      }
    }
  }

}
