<?php

namespace Drupal\folder\Plugin\views\argument;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\folder\FolderStorage;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler for basic folder fid.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("folder")
 */
class Folder extends NumericArgument implements ContainerFactoryPluginInterface {

  protected FolderStorage $folderStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->folderStorage = $container->get('entity_type.manager')->getStorage('folder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function title() {
    if ($this->argument) {
      $folder = $this->folderStorage->load($this->argument);
      if ($folder) {
        return $folder->label();
      }
    }

    return parent::title();
  }

}
