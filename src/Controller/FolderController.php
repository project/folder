<?php

namespace Drupal\folder\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\folder\Entity\FolderInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the add-page and title callbacks for folders.
 */
class FolderController extends EntityController {

  protected RouteMatchInterface $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function addTitle($entity_type_id) {
    return $this->t('Add folder');
  }

  /**
   * {@inheritdoc}
   */
  public function addPage($entity_type_id) {
    $entity_type = $this->entityTypeManager->getDefinition('folder');
    $bundles = $this->entityTypeBundleInfo->getBundleInfo('folder');
    $bundle_entity_type_id = 'folder_type';
    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];

    $bundle_argument = $bundle_entity_type_id;
    $bundle_entity_type = $this->entityTypeManager->getDefinition('folder_type');
    $build['#cache']['tags'] = $bundle_entity_type->getListCacheTags();

    // Build the message shown when there are no bundles.
    $link_text = $this->t('Add a new folder.');
    $link_route_name = 'entity.folder_type.add_form';
    $build['#add_bundle_message'] = $this->t('There is no @entity_type yet. @add_link', [
      '@entity_type' => $bundle_entity_type->getSingularLabel(),
      '@add_link' => Link::createFromRoute($link_text, $link_route_name)->toString(),
    ]);

    // Filter out the bundles the user doesn't have access to.
    $user = $this->routeMatch->getParameter('user');
    $user = $user instanceof AccountInterface ? $user : User::load($user);
    $access_control_handler = $this->entityTypeManager->getAccessControlHandler($entity_type_id);
    foreach ($bundles as $bundle_name => $bundle_info) {
      $access = $access_control_handler->createAccess($bundle_name, $user, [], TRUE);
      if (!$access->isAllowed()) {
        unset($bundles[$bundle_name]);
      }
      $this->renderer->addCacheableDependency($build, $access);
    }
    // Add descriptions from the bundle entities.
    $bundles = $this->loadBundleDescriptions($bundles, $bundle_entity_type);

    $form_route_name = 'entity.folder.add_form';
    $parameters = ['user' => $user->id()];
    // Redirect if there's only one bundle available.
    if (count($bundles) == 1) {
      $bundle_names = array_keys($bundles);
      $bundle_name = reset($bundle_names);
      return $this->redirect($form_route_name, [
        $bundle_argument => $bundle_name,
      ] + $parameters);
    }
    // Prepare the #bundles array for the template.
    foreach ($bundles as $bundle_name => $bundle_info) {
      $build['#bundles'][$bundle_name] = [
        'label' => $bundle_info['label'],
        'description' => $bundle_info['description'] ?? '',
        'add_link' => Link::createFromRoute($bundle_info['label'], $form_route_name, [
          $bundle_argument => $bundle_name,
        ] + $parameters),
      ];
    }

    return $build;
  }

  /**
   * Route callback for teaser content.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user who owns the folder.
   * @param \Drupal\folder\Entity\FolderInterface $folder
   *   The folder to be viewed.
   * @param string $nojs
   *   nojs when ajax shouldn't be used, ajax when ajax should be used.
   *
   * @return array|\Drupal\Core\Ajax\AjaxResponse
   *   The render array or ajax response.
   */
  public function teaserContent(AccountInterface $user, FolderInterface $folder, string $nojs = 'nojs') {
    $folder_view_builder = $this->entityTypeManager->getViewBuilder('folder');
    $build = $folder_view_builder->view($folder, 'teaser');
    if ($nojs == 'ajax') {
      $build['#attributes']['data-folders-folder-container'] = TRUE;
      $response = new AjaxResponse();
      $response->addCommand(new ReplaceCommand('[data-folders-folder-container]', $build));
      return $response;
    }
    return $build;
  }

}
