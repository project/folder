<?php

namespace Drupal\folder;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Sql\TableMappingInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\folder\Entity\FolderInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorage;
use Drupal\taxonomy\VocabularyInterface;

/**
 * Defines storage class for folders.
 */
class FolderStorage extends TermStorage implements FolderStorageInterface {

  /**
   * The type of hierarchy allowed within a folder type.
   *
   * The only possible value is:
   *   - VocabularyInterface::HIERARCHY_DISABLED: No parents.
   *   - VocabularyInterface::HIERARCHY_SINGLE: Single parent.
   *
   * @var int[]
   *   An array of the possible value above, keyed by folder type ID.
   */
  protected array $folderTypeHierarchyType;

  /**
   * {@inheritdoc}
   */
  public function create(array $values = []) {
    if (empty($values['parent']) || $values['parent'] === [0]) {
      $route_match = \Drupal::routeMatch();
      $parent = $route_match->getParameter('parent');
      if ($parent instanceof FolderInterface) {
        $values['parent'] = [$parent->id()];
      }
      elseif (is_numeric($parent) && $parent > 0) {
        $values['parent'] = [$parent];
      }
    }
    return parent::create($values);
  }

  /**
   * Load multiple folders by user.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user.
   *
   * @return array
   *   The folders.
   *
   */
  public function loadMultipleByUser(AccountInterface $user): array {
    $query = $this->getQuery()
      ->condition('uid', $user->id())
      ->sort('weight');
    $result = $query->execute();
    return $result ? $this->loadMultiple($result) : [];
  }

  /**
   * Returns all-children terms of this term.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   A list of children folder entities keyed by term ID.
   */
  public function getChildren(TermInterface $term = NULL, int $uid = 0) {
    $query = \Drupal::entityQuery('folder')
      ->accessCheck(TRUE);
    if ($term) {
      $query->condition('parent', $term->id());
    }
    else {
      $group = $query->orConditionGroup();
      $group->notExists('parent');
      $group->condition('parent.target_id', 0);
      $query->condition($group);
    }

    if ($uid) {
      $query->condition('uid', $uid);
    }
    return static::loadMultiple($query->execute());
  }

  /**
   * Returns a list of parents of this term.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   The parent taxonomy term entities keyed by term ID. If this term has a
   *   <root> parent, that item is keyed with 0 and will have NULL as value.
   *
   * @internal
   * @todo Refactor away when TreeInterface is introduced.
   */
  protected function getParents(TermInterface $term) {
    $parents = $ids = [];
    // Cannot use $this->get('parent')->referencedEntities() here because that
    // strips out the '0' reference.
    foreach ($term->get('parent') as $item) {
      if ($item->target_id == 0) {
        // The <root> parent.
        $parents[0] = NULL;
        continue;
      }
      $ids[] = $item->target_id;
    }

    // @todo Better way to do this? AND handle the NULL/0 parent?
    // Querying the terms again so that the same access checks are run when
    // getParents() is called as in Drupal version prior to 8.3.
    $loaded_parents = [];

    if ($ids) {
      $query = \Drupal::entityQuery('folder')
        ->accessCheck(TRUE)
        ->condition('fid', $ids, 'IN');

      $loaded_parents = static::loadMultiple($query->execute());
    }

    return $parents + $loaded_parents;
  }

  /**
   * {@inheritdoc}
   */
  public function loadTree($vid, $parent = 0, $max_depth = NULL, $load_entities = FALSE, $uid = 0) {
    $folder_type_id = $vid;
    $cache_key = implode(':', func_get_args());
    if (!isset($this->trees[$cache_key])) {
      // We cache trees, so it's not CPU-intensive to call on a term and its
      // children, too.
      if (!isset($this->treeChildren[$folder_type_id])) {
        $this->treeChildren[$folder_type_id] = [];
        $this->treeParents[$folder_type_id] = [];
        $this->treeTerms[$folder_type_id] = [];
        $query = $this->database->select($this->getDataTable(), 'f');
        $query->addTag('folder_access')
          ->fields('f')
          ->condition('f.type', $folder_type_id);
        if ($uid) {
          $query->condition('f.uid', $uid);
        }
        $result = $query->condition('f.default_langcode', 1)
          ->orderBy('f.weight')
          ->orderBy('f.name')
          ->execute();
        foreach ($result as $folder) {
          $this->treeChildren[$folder_type_id][$folder->parent][] = $folder->fid;
          $this->treeParents[$folder_type_id][$folder->fid][] = $folder->parent;
          $this->treeTerms[$folder_type_id][$folder->fid] = $folder;
        }
      }

      // Load full entities, if necessary. The entity controller statically
      // caches the results.
      $folder_entities = [];
      if ($load_entities) {
        $folder_entities = $this->loadMultiple(array_keys($this->treeTerms[$folder_type_id]));
      }

      $max_depth = (!isset($max_depth)) ? count($this->treeChildren[$folder_type_id]) : $max_depth;
      $tree = [];

      // Keeps track of the parents we have to process, the last entry is used
      // for the next processing step.
      $process_parents = [];
      $process_parents[] = $parent;

      // Loops over the parent terms and adds its children to the tree array.
      // Uses a loop instead of a recursion, because it's more efficient.
      while (count($process_parents)) {
        $parent = array_pop($process_parents);
        // The number of parents determines the current depth.
        $depth = count($process_parents);
        if ($max_depth > $depth && !empty($this->treeChildren[$folder_type_id][$parent])) {
          $has_children = FALSE;
          $child = current($this->treeChildren[$folder_type_id][$parent]);
          do {
            if (empty($child)) {
              break;
            }
            $folder = $load_entities ? $folder_entities[$child] : $this->treeTerms[$folder_type_id][$child];
            if (isset($this->treeParents[$folder_type_id][$load_entities ? $folder->id() : $folder->fid])) {
              // Clone the term so that the depth attribute remains correct
              // in the event of multiple parents.
              $folder = clone $folder;
            }
            $folder->depth = $depth;
            if (!$load_entities) {
              unset($folder->parent);
            }
            $fid = $load_entities ? $folder->id() : $folder->fid;
            $folder->parents = $this->treeParents[$folder_type_id][$fid];
            $tree[] = $folder;
            if (!empty($this->treeChildren[$folder_type_id][$fid])) {
              $has_children = TRUE;

              // We have to continue with this parent later.
              $process_parents[] = $parent;
              // Use the current term as parent for the next iteration.
              $process_parents[] = $fid;

              // Reset pointers for child lists because we step in there more
              // often with multi parents.
              reset($this->treeChildren[$folder_type_id][$fid]);
              // Move pointer so that we get the correct term the next time.
              next($this->treeChildren[$folder_type_id][$parent]);
              break;
            }
          } while ($child = next($this->treeChildren[$folder_type_id][$parent]));

          if (!$has_children) {
            // We processed all terms in this hierarchy-level, reset pointer
            // so that this function works the next time it gets called.
            reset($this->treeChildren[$folder_type_id][$parent]);
          }
        }
      }
      $this->trees[$cache_key] = $tree;
    }
    return $this->trees[$cache_key];
  }

  /**
   * {@inheritdoc}
   */
  public function loadContentFolders(ContentEntityInterface $entity, $uid, $folder_type_id = 'folder', $load_entities = FALSE): array {
    $query = $this->database->select($this->getDataTable(), 'f');
    $query->join('folder__content', 'c', '[f].[fid] = [c].[entity_id]');
    $query->addTag('folder_access')
      ->fields('f', ['fid', 'uid', 'type'])
      ->fields('c', ['content_target_id', 'content_target_type']);
    $results = $query->groupBy('f.fid')
      ->condition('f.type', $folder_type_id)
      ->condition('f.uid', $uid)
      ->condition('c.content_target_id', $entity->id())
      ->condition('c.content_target_type', $entity->getEntityTypeId())
      ->execute()
      ->fetchAllAssoc('fid');

    return $results && $load_entities ? $this->loadMultiple(array_keys($results)) : array_keys($results);
  }

  /**
   * {@inheritdoc}
   */
  public function resetWeights($vid) {
    $this->database->update($this->getDataTable())
      ->fields(['weight' => 0])
      ->condition('tid', $vid)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabularyHierarchyType($vid) {
    return $this->getFolderTypeHierarchyType($vid);
  }

  /**
   * Returns the hierarchy type for a specific folder type ID.
   *
   * @param string $folder_type_id
   *   Folder type ID to retrieve the hierarchy type for.
   *
   * @return int
   *   The folder type hierarchy.
   *   Possible values:
   *    - VocabularyInterface::HIERARCHY_DISABLED: No parents.
   *    - VocabularyInterface::HIERARCHY_SINGLE: Single parent.
   *
   * @throws \Drupal\Core\Entity\Sql\SqlContentEntityStorageException
   */
  public function getFolderTypeHierarchyType(string $folder_type_id): int {
    // Return early if we already computed this value.
    if (isset($this->folderTypeHierarchyType[$folder_type_id])) {
      $this->vocabularyHierarchyType[$folder_type_id] = $this->folderTypeHierarchyType[$folder_type_id];
      return $this->folderTypeHierarchyType[$folder_type_id];
    }

    $parent_field_storage = $this->entityFieldManager->getFieldStorageDefinitions($this->entityTypeId)['parent'];
    $table_mapping = $this->getTableMapping();

    $target_id_column = $table_mapping->getFieldColumnName($parent_field_storage, 'target_id');
    $delta_column = $table_mapping->getFieldColumnName($parent_field_storage, TableMappingInterface::DELTA);

    $query = $this->database->select($table_mapping->getFieldTableName('parent'), 'p');
    $query->addExpression("MAX([$target_id_column])", 'max_parent_id');
    $query->addExpression("MAX([$delta_column])", 'max_delta');
    $query->condition('bundle', $folder_type_id);

    $result = $query->execute()->fetchAll();

    // If all the folders have the same parent, the parent can only be root (0).
    if ((int) $result[0]->max_parent_id === 0) {
      $this->folderTypeHierarchyType[$folder_type_id] = VocabularyInterface::HIERARCHY_DISABLED;
    }
    else {
      $this->folderTypeHierarchyType[$folder_type_id] = VocabularyInterface::HIERARCHY_MULTIPLE;
    }

    $this->vocabularyHierarchyType[$folder_type_id] = $this->folderTypeHierarchyType[$folder_type_id];
    return $this->folderTypeHierarchyType[$folder_type_id];
  }

}
