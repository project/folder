<?php

namespace Drupal\folder;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\taxonomy\TermStorageInterface;

/**
 * Interface FolderStorageInterface
 */
interface FolderStorageInterface extends TermStorageInterface {

  /**
   * Loads content folders for a given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity for which to load content folders.
   * @param mixed $uid
   *   The user ID associated with the content folders.
   * @param string $folder_type_id
   *   (optional) The folder type ID to load, defaults to 'folder'.
   * @param bool $load_entities
   *   (optional) Whether to load the entities associated with the content folders,
   *   defaults to FALSE.
   *
   * @return int[]|\Drupal\folder\Entity\FolderInterface[]
   *   An array of content folder ids or entities.
   */
  public function loadContentFolders(ContentEntityInterface $entity, int $uid, string $folder_type_id = 'folder', bool $load_entities = FALSE): array;

}

