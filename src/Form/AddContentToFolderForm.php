<?php

namespace Drupal\folder\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AddContentToFolderForm extends FormBase {

  use RedirectDestinationTrait;

  const NEW_FOLDER = '_new_folder';

  /**
   * The entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  protected $entityToAdd;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityRepository = $container->get('entity.repository');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'folder_add_content_item_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, AccountInterface $user = NULL, string $entity_type_id = '', int $entity_id = 0) {
    if (!$entity_type_id || !$entity_id) {
      throw new NotFoundHttpException();
    }

    $user = $user ?: $this->getRouteMatch()->getParameter('user');
    if (!$user) {
      $user = \Drupal::currentUser();
    }
    elseif (is_numeric($user)) {
      $user_storage = $this->entityTypeManager->getStorage('user');
      $user = $user_storage->load($user);
    }

    if (!($user instanceof AccountInterface)) {
      throw new NotFoundHttpException();
    }

    $form['#wrapper_id'] = 'folder_add_content_item_form__wrapper';
    $form['#prefix'] = '<div id="' . $form['#wrapper_id'] . '">';
    $form['#suffix'] = '</div>';

    $form['#user'] = $user;

    /** @var \Drupal\folder\FolderStorage $folder_storage */
    $folder_storage = $this->entityTypeManager->getStorage('folder');
    $tree = $folder_storage->loadTree('folder', 0, NULL, FALSE, $user->id());
    $entity = $this->getEntityToAdd($entity_type_id, $entity_id);
    $content_folders = $folder_storage->loadContentFolders($entity, $user->id());
    $options = [
      static::NEW_FOLDER => $this->t('- Add to a new folder -'),
    ];
    $content_folder_labels = [];
    foreach ($tree as $item) {
      $folder = $folder_storage->load($item->fid);
      $folder = $this->entityRepository->getTranslationFromContext($folder);
      if (in_array($item->fid, $content_folders)) {
        $content_folder_labels[$item->fid] = $folder->label();
      }
      $options[$item->fid] = str_repeat('-', $item->depth) . $folder->label();;
    }

    $t_args = [
      '@entity' => $entity->label(),
      '@folders' => implode(', ', array_values($content_folder_labels)),
    ];
    $form['content_folders'] = [
      '#markup' => '<p>' . $this->t('The content you are about to add to a folder is already added in the following folder(s): @folders', $t_args) . '</p>',
      '#access' => !empty($content_folder_labels),
      '#suffix' => '<hr />',
    ];
    // @todo refactor the select element above to disable some folder options when
    // https://www.drupal.org/project/drupal/issues/342316 is going to be committed.
    $form['fid'] = [
      '#type' => 'select',
      '#title' => $this->t('Please select the folder to add the content to'),
      '#description' => $content_folder_labels ? $this->t('Note that selecting folders in which "@entity" is already added in, will cause a validation error upon submission. You can\'t add the same content in more than one folder.', $t_args) : '',
      '#options' => $options,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::folderSelectionCallback',
        'wrapper' => 'folders--selected-folder-content-container',
      ],
    ];
    $form['new_folder_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('New folder name'),
      '#states' => [
        'required' => [
          ':input[name=fid]' => ['value' => '_new_folder'],
        ],
        'visible' => [
          ':input[name=fid]' => ['value' => '_new_folder'],
        ],
      ],
    ];

    $form['selected_folder'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'folders--selected-folder-content-container'],
    ];
    $form['selected_folder']['content'] = $this->buildSelectedFolderContent($form_state);

    $form['#folder_content_to_add'] = [
      'entity_type_id' => $entity_type_id,
      'entity_id' => $entity_id,
      'user_id' => $user->id(),
    ];

    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->addCacheableDependency($user);
    $cacheable_metadata->applyTo($form['fid']);

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add to folder'),
      '#attributes' => ['class' => ['use-ajax']],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];

    $entity = $this->getEntityToAdd($entity_type_id, $entity_id);

    $form['actions']['back_to_content'] = [
      '#type' => 'link',
      '#title' => $this->t('Back to content'),
      '#url' => $entity->toUrl(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $fid = $form_state->getValue('fid');

    $folder_storage = $this->entityTypeManager->getStorage('folder');
    if ($fid === static::NEW_FOLDER) {
      $folder_name = $form_state->getValue('new_folder_name');
      if (empty($folder_name)) {
        $form_state->setError($form['new_folder_name'], $this->t('Please enter a folder name.'));
        return;
      }

      /** @var \Drupal\folder\Entity\FolderInterface $selected_folder */
      $selected_folder = $folder_storage->create([
        'name' => $folder_name,
        'type' => 'folder',
        'uid' => $form['#folder_content_to_add']['user_id'],
      ]);
      $violations = $selected_folder->validate();
      if ($violations->count()) {
        foreach ($violations as $violation) {
          $form_state->setError($form['new_folder_name'], $violation->getMessage());
        }
        return;
      }

      // Saving the folder and changing the selected folder id in the form state storage.
      $selected_folder->save();
      $form_state->setValue('fid', $selected_folder->id());
      return;
    }

    /** @var \Drupal\folder\Entity\FolderInterface $selected_folder */
    $selected_folder = $folder_storage->load($fid);
    if (!$selected_folder) {
      $form_state->setErrorByName('fid', $this->t('We couldn\'t retrieve the selected folder.'));
      return;
    }

    $entity = $this->getEntityToAdd($form['#folder_content_to_add']['entity_type_id'], $form['#folder_content_to_add']['entity_id']);
    if ($selected_folder->hasContentItem($entity)) {
      $form_state->setErrorByName('fid', $this->t('The @entity content has been already added to @folder folder.', [
        '@entity' => $entity->label(),
        '@folder' => $selected_folder->label(),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\folder\Entity\FolderInterface $selected_folder */
    $selected_folder = $this->entityTypeManager
      ->getStorage('folder')
      ->load($form_state->getValue('fid'));

    $entity = $this->getEntityToAdd($form['#folder_content_to_add']['entity_type_id'], $form['#folder_content_to_add']['entity_id']);
    $selected_folder->addContentItem($entity);
    $selected_folder->setRevisionLogMessage((string) $this->t('@entity content item was added to the folder.', [
      '@entity' => $entity->label(),
    ]));
    $selected_folder->setRevisionUser($form['#user']);
    $selected_folder->save();

    $this->messenger()->addMessage($this->t('"@entity" content has been successfully added to "@folder" folder.', [
      '@entity' => $entity->label(),
      '@folder' => $selected_folder->label(),
    ]));
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitModalFormAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->hasAnyErrors()) {
      // The status messages that will contain any form errors.
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -100,
      ];
      $response->addCommand(new ReplaceCommand('#' . $form['#wrapper_id'], $form));
    }
    else {
      $response->addCommand(new RedirectCommand($this->getRedirectUrl($form, TRUE, TRUE)));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

  /**
   * Folder selection ajax callback.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The array structure of the folder view container.
   */
  public function folderSelectionCallback(array $form, FormStateInterface $form_state): array {
    return $form['selected_folder'];
  }

  /**
   * Title callbacks for the form.
   *
   * @param \Drupal\Core\Session\AccountInterface|NULL $user
   *   The user account.
   * @param string $entity_type_id
   *   The entity type of the content to add to the folder.
   * @param int $entity_id
   *   The entity id of the content to add to the folder.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function title(AccountInterface $user = NULL, string $entity_type_id = '', int $entity_id = 0) {
    if (!$entity_type_id || !$entity_id || !$user || $user->isAnonymous()) {
      return $this->t('Add content to folder');
    }

    $entity = $this->getEntityToAdd($entity_type_id, $entity_id);
    return  $this->t('Add "@entity" content to one of @user folders', [
      '@entity' => $entity->label(),
      '@user' => $user->getDisplayName(),
    ]);
  }

  /**
   * Gets the entity type id.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param int $entity_id
   *   The entity id.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The entity content to add.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityToAdd(string $entity_type_id, int $entity_id): ?ContentEntityInterface {
    if ($this->entityToAdd) {
      return $this->entityToAdd;
    }

    if (!$entity_type_id || !$entity_id) {
      $this->entityToAdd = NULL;
      return $this->entityToAdd;
    }
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $entity = $storage->load($entity_id);
    $this->entityToAdd = $this->entityRepository->getTranslationFromContext($entity);
    return $this->entityToAdd;
  }

  /**
   * Build the selected folder content.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state to get the selected folder id.
   *
   * @return array
   *   The built view of the folder.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildSelectedFolderContent(FormStateInterface $form_state): array {
    $fid = $form_state->getValue('fid', 0);
    if ($fid && $fid !== static::NEW_FOLDER) {
      $folder_view_builder = $this->entityTypeManager->getViewBuilder('folder');
      $folder = $this->entityTypeManager->getStorage('folder')->load($fid);
      return $folder_view_builder->view($folder, 'add_content_to_folder_form');
    }
    return [];
  }

  /**
   * Gets the redirect url.
   *
   * @param array $form
   *   The form array to extract needed metadata.
   * @param bool $as_string
   *   Whether to return the redirect url as string.
   * @param bool $absolute
   *   Whether the url should be absolute.
   *
   * @return string|\Drupal\Core\Url
   *   Url object or string.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function getRedirectUrl(array $form, bool $as_string = FALSE, bool $absolute = FALSE): string|Url {
    $options = ['absolute' => $absolute];
    if ($this->getRequest()->query->has('destination') && $destination = $this->getRedirectDestination()->get()) {
      $redirect_url = Url::fromUserInput($destination, $options);
    }
    else {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $this->getEntityToAdd($form['#folder_content_to_add']['entity_type_id'], $form['#folder_content_to_add']['entity_id']);
      $redirect_url = $entity->toUrl('canonical', $options);
    }
    return $as_string ? $redirect_url->toString() : $redirect_url;
  }

}
