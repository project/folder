<?php

namespace Drupal\folder\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Default form handler for the folder.
 */
class FolderForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\folder\Entity\FolderInterface $folder */
    $folder = $this->entity;
    $folder_type_storage = $this->entityTypeManager->getStorage('folder_type');
    $owner_id = $folder->getOwnerId();
    /** @var \Drupal\folder\FolderStorage $folder_storage */
    $folder_storage = $this->entityTypeManager->getStorage('folder');
    $folder_type = $folder_type_storage->load($folder->bundle());

    $parent_id = $folder->getParentId();

    $form_state->set(['folder', 'parent'], $parent_id);
    $form_state->set(['folder', 'type'], $folder_type);

    $exclude = [];
    if (!$folder->isNew()) {
      $children = $folder_storage->loadTree($folder_type->id(), $folder->id(), NULL, FALSE, $owner_id);

      // A folder can't be the child of itself, nor of its children.
      foreach ($children as $child) {
        $exclude[] = $child->fid;
      }
      $exclude[] = $folder->id();
    }

    $tree = $folder_storage->loadTree($folder_type->id(), 0, NULL, FALSE, $owner_id);
    $options = ['<' . $this->t('root') . '>'];
    if (empty($parent_id)) {
      $parent_id = 0;
    }

    foreach ($tree as $item) {
      if (!in_array($item->fid, $exclude)) {
        $options[$item->fid] = str_repeat('-', $item->depth) . $item->name;
      }
    }

    $form['relations'] = [
      '#type' => 'details',
      '#title' => $this->t('Move folder'),
      '#open' => FALSE,
      '#weight' => 10,
    ];

    $form['relations']['parent'] = [
      '#type' => 'select',
      '#title' => $this->t('Parent'),
      '#options' => $options,
      '#default_value' => $parent_id,
    ];
    $form['relations']['weight'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Weight'),
      '#size' => 6,
      '#default_value' => $folder->getWeight(),
      '#description' => $this->t('Folders are displayed in ascending order by weight.'),
      '#required' => TRUE,
    ];

    $form['type'] = [
      '#type' => 'value',
      '#value' => $folder_type->id(),
    ];
    $form['fid'] = [
      '#type' => 'value',
      '#value' => $folder->id(),
    ];
    $form['uid'] = [
      '#type' => 'value',
      '#value' => $owner_id,
    ];
    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Ensure numeric values.
    if ($form_state->hasValue('weight') && !is_numeric($form_state->getValue('weight'))) {
      $form_state->setErrorByName('weight', $this->t('Weight value must be numeric.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $folder = parent::buildEntity($form, $form_state);

    // Prevent leading and trailing spaces in folder names.
    $folder->setName(trim($folder->getName()));

    // Assign parents with proper delta values starting from 0.
    $folder->parent = array_values((array)$form_state->getValue('parent'));

    return $folder;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditedFieldNames(FormStateInterface $form_state) {
    return array_merge(['parent', 'weight'], parent::getEditedFieldNames($form_state));
  }

  /**
   * {@inheritdoc}
   */
  protected function flagViolations(EntityConstraintViolationListInterface $violations, array $form, FormStateInterface $form_state) {
    // Manually flag violations of fields aren't handled by the form display.
    // This is necessary as entity form displays only flag violations for fields
    // contained in the display.
    // @see ::form()
    foreach ($violations->getByField('parent') as $violation) {
      $form_state->setErrorByName('parent', $violation->getMessage());
    }
    foreach ($violations->getByField('weight') as $violation) {
      $form_state->setErrorByName('weight', $violation->getMessage());
    }

    parent::flagViolations($violations, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\folder\Entity\FolderInterface $folder */
    $folder = $this->entity;

    $result = $folder->save();

    $edit_link = $folder->toLink($this->t('Edit'), 'edit-form')->toString();
    $view_link = $folder->toLink()->toString();
    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created new folder %folder.', ['%folder' => $view_link]));
        $this->logger('folder')->notice('Created new folder %folder.', ['%folder' => $folder->getName(), 'link' => $edit_link]);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('Updated folder %folder.', ['%folder' => $view_link]));
        $this->logger('folder')->notice('Updated folder %folder.', ['%folder' => $folder->getName(), 'link' => $edit_link]);
        break;
    }

    $form_state->setRedirect('entity.folder.collection', [
      'user' => $folder->getOwnerId(),
    ]);

    // Root doesn't count if it's the only parent.
    if ($form_state->hasValue(['parent', 0])) {
      $form_state->setValue('parent', []);
    }

    $form_state->setValue('fid', $folder->id());
    $form_state->set('fid', $folder->id());
  }

}
