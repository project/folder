<?php

namespace Drupal\folder\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dynamic_entity_reference\Plugin\Field\FieldType\DynamicEntityReferenceItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Config form instance for the folder module.
 */
class FolderConfigForm extends ConfigFormBase {

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected CacheTagsInvalidatorInterface $cacheTagsInvalidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->setCacheTagsInvalidator($container->get('cache_tags.invalidator'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'folder_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['folder.configurations'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $configurations = $this->config('folder.configurations');
    $form['view_content_in_teaser'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('View folder content in teaser'),
      '#description' => $this->t('If enable content of a folder in the overview form will display in the sidebar teaser beside the table instead of a new page.'),
      '#default_value' => !empty($configurations->get('view_content_in_teaser')),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $configurations = $this->config('folder.configurations');

    $current_view_content_in_teaser = $configurations->get('view_content_in_teaser');
    $updated_view_content_in_teaser = !empty($form_state->getValue('view_content_in_teaser'));

    if ($current_view_content_in_teaser !== $updated_view_content_in_teaser) {
      // Invalidate cache tags to refresh any rendering relying on these
      // configurations.
      $this->cacheTagsInvalidator->invalidateTags($configurations->getCacheTags());
    }

    $configurations->set('view_content_in_teaser', $updated_view_content_in_teaser)->save();
  }

  /**
   * Sets cache tags invalidator.
   *
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator service to set.
   */
  private function setCacheTagsInvalidator(CacheTagsInvalidatorInterface $cache_tags_invalidator): void {
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

}
