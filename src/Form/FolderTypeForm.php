<?php

namespace Drupal\folder\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\dynamic_entity_reference\Plugin\Field\FieldType\DynamicEntityReferenceItem;
use Drupal\folder\FolderTypeStorageInterface;
use Drupal\language\Entity\ContentLanguageSettings;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FolderTypeForm extends BundleEntityFormBase {

  protected FolderTypeStorageInterface $folderTypeStorage;

  /**
   * The entity type repository.
   *
   * @var \Drupal\Core\Entity\EntityTypeRepositoryInterface
   */
  protected EntityTypeRepositoryInterface $entityTypeRepository;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->folderTypeStorage = $instance->entityTypeManager->getStorage('folder_type');
    $instance->entityTypeRepository = $container->get('entity_type.repository');
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\folder\Entity\FolderTypeInterface $folder_type */
    $folder_type = $this->entity;
    $form['#title'] = $folder_type->isNew() ? $this->t('Add folder type') : $this->t('Edit folder type');

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $folder_type->label(),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $folder_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['name'],
      ],
    ];
    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => $folder_type->getDescription(),
    ];

    $form['langcode'] = [
      '#type' => 'language_select',
      '#title' => $this->t('Folder type language'),
      '#languages' => LanguageInterface::STATE_ALL,
      '#default_value' => $folder_type->language()->getId(),
    ];
    if ($this->moduleHandler->moduleExists('language')) {
      $form['default_folders_language'] = [
        '#type' => 'details',
        '#title' => $this->t('Folder language'),
        '#open' => TRUE,
      ];
      $form['default_folders_language']['default_language'] = [
        '#type' => 'language_configuration',
        '#entity_information' => [
          'entity_type' => 'folder',
          'bundle' => $folder_type->id(),
        ],
        '#default_value' => ContentLanguageSettings::loadByEntityTypeBundle('folder', $folder_type->id()),
      ];
    }
    // Set the hierarchy to "multiple parents" by default. This simplifies the
    // folder type form and standardizes the folder form.
    $form['hierarchy'] = [
      '#type' => 'value',
      '#value' => '0',
    ];
    $form = $this->buildAllowedContentForm($form, $form_state);
    $form = parent::form($form, $form_state);
    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $updated_allowed_content = [];
    // Removing the remove button in values.
    foreach ($form_state->getValue('allowed_content') as $index => $allowed_content_item_data) {
      unset($allowed_content_item_data['remove']);
      $updated_allowed_content[] = $allowed_content_item_data;
    }
    $form_state->setValue('allowed_content', $updated_allowed_content);
    return parent::buildEntity($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $folder_type = $this->entity;

    // Prevent leading and trailing spaces in folder_type names.
    $folder_type->set('name', trim($folder_type->label()));

    $status = $folder_type->save();
    $edit_link = $folder_type->toLink($this->t('Edit'), 'edit-form')->toString();
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created new folder type %name.', ['%name' => $folder_type->label()]));
        $this->logger('folder')->notice('Created new folder type %name.', ['%name' => $folder_type->label(), 'link' => $edit_link]);
        $form_state->setRedirectUrl($folder_type->toUrl('collection'));
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('Updated folder type %name.', ['%name' => $folder_type->label()]));
        $this->logger('folder')->notice('Updated folder type %name.', ['%name' => $folder_type->label(), 'link' => $edit_link]);
        $form_state->setRedirectUrl($folder_type->toUrl('collection'));
        break;
    }

    $form_state->setValue('vid', $folder_type->id());
    $form_state->set('vid', $folder_type->id());
  }

  /**
   * Determines if the folder type already exists.
   *
   * @param string $id
   *   The folder ID.
   *
   * @return bool
   *   TRUE if the folder type exists, FALSE otherwise.
   */
  public function exists(string $id): bool {
    $action = $this->folderTypeStorage->load($id);
    return !empty($action);
  }

  public function buildAllowedContentForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\folder\Entity\FolderTypeInterface $folder_type */
    $folder_type = $this->entity;
    $allowed_content = $folder_type->getAllowedContent();
    $user_input = $form_state->getUserInput();
    $entity_type_options = $this->contentEntityTypeOptions();

    $folder_type_allowed_content = (array) $form_state->get('folder_type_allowed_content');
    if (empty($folder_type_allowed_content )) {
      $folder_type_allowed_content = $allowed_content ? array_keys($allowed_content) : ['_new'];
      $form_state->set('folder_type_allowed_content', $folder_type_allowed_content);
    }

    $wrapper_id = Html::getUniqueId('folder-type--allowed-content');
    $table_drag_grouping = 'folder-type--allowed-content--order-weight';
    $form['allowed_content'] = [
      '#type' => 'table',
      '#header' => [
        ['data' => $this->t('Entity type'), 'colspan' => 2],
        $this->t('Bundle'),
        $this->t('Weight'),
        $this->t('Operations'),
      ],
      '#tabledrag' => [[
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => $table_drag_grouping,
      ]],
      '#input' => FALSE,
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    ];
    $max_weight = count($folder_type_allowed_content);

    foreach ($folder_type_allowed_content as $index => $position) {
      $allowed_content_item_form = &$form['allowed_content'][$index];

      $allowed_content_item_form['#attributes']['class'][] = 'draggable';
      $allowed_content_item_form['tabledrag'] = [
        '#markup' => '',
      ];

      $entity_type_id = $form_state->getValue([
        'allowed_content',
        $index,
        'entity_type_id',
      ], '');

      $allowed_content_item_form['entity_type_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Entity type'),
        '#title_display' => 'invisible',
        '#options' => $entity_type_options,
        '#required' => TRUE,
        '#allowed_content_item_index' => $index,
        '#ajax' => [
          'callback' => '::allowedContentItemsAjaxCallback',
          'wrapper' => $wrapper_id,
        ],
      ];
      $allowed_content_item_form['bundle'] = [
        '#type' => 'select',
        '#title' => $this->t('Bundle'),
        '#title_display' => 'invisible',
        '#options' => ['' => $this->t('Select a valid entity')],
        '#attributes' => ['disabled' => TRUE],
        '#required' => TRUE,
      ];

      if ($position === '_new') {
        $default_weight = $max_weight;
        $remove_access = TRUE;
      }
      else {
        $default_value = $allowed_content[$position];
        $allowed_content_item_form['entity_type_id']['#default_value'] = $entity_type_id = $default_value['entity_type_id'];
        $default_weight = $default_value['weight'];
        $remove_access = FALSE;
      }

      if ($entity_type_id) {
        $bundles = $this->contentEntityTypeBundleOptions($entity_type_id);
        $allowed_content_item_form['bundle']['#options'] = ['' => '- None -'] + $bundles;
        $allowed_content_item_form['bundle']['#default_value'] = $default_value['bundle'] ?? '';
        $allowed_content_item_form['bundle']['#attributes']['disabled'] = FALSE;
      }

      $allowed_content_item_form['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight'),
        '#title_display' => 'invisible',
        '#delta' => $max_weight,
        '#default_value' => $default_weight,
        '#attributes' => ['class' => [$table_drag_grouping]],
      ];

      if (isset($user_input['allowed_content'][$index])) {
        $input_weight = $user_input['allowed_content'][$index]['weight'];
        // Make sure the weight is not out of bounds due to removals.
        if ($user_input['allowed_content'][$index]['weight'] > $max_weight) {
          $input_weight = $max_weight;
        }
        // Reflect the updated user input on the element.
        $allowed_content_item_form['weight']['#value'] = $input_weight;
        $allowed_content_item_form['#weight'] = $input_weight;
      }
      else {
        $allowed_content_item_form['#weight'] = $default_weight;
      }

      $allowed_content_item_form['remove'] = [
        '#type' => 'submit',
        '#name' => 'remove_allowed_content_item_' . $index,
        '#value' => $this->t('Remove'),
        '#limit_validation_errors' => [],
        '#submit' => ['::removeDefaultDisplayColorItemSubmit'],
        '#allowed_content_item_index' => $index,
        '#ajax' => [
          'callback' => '::allowedContentItemsAjaxCallback',
          'wrapper' => $wrapper_id,
        ],
        '#access' => !$remove_access,
      ];
    }

    // Sort the values by weight. Ensures weight is preserved on ajax refresh.
    uasort($form['allowed_content'], [
      '\Drupal\Component\Utility\SortArray',
      'sortByWeightProperty',
    ]);

    $form['allowed_content']['_add_new'] = ['#tree' => FALSE];
    $form['allowed_content']['_add_new']['item'] = [
      '#type' => 'container',
      '#wrapper_attributes' => ['colspan' => 6],
      '#tree' => FALSE,
    ];
    $form['allowed_content']['_add_new']['item']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add a new allowed content'),
      '#submit' => ['::addAllowedContentItemSubmit'],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => '::allowedContentItemsAjaxCallback',
        'wrapper' => $wrapper_id,
      ],
    ];

    return $form;
  }

  /**
   * Handles ajax refresh of the allowed content table.
   */
  public function allowedContentItemsAjaxCallback(array $form, FormStateInterface $form_state): array {
    return $form['allowed_content'];
  }

  /**
   * Submit callback to add an allowed content item.
   */
  public function addAllowedContentItemSubmit(array $form, FormStateInterface $form_state): void {
    $folder_type_allowed_content = (array) $form_state->get('folder_type_allowed_content');
    $folder_type_allowed_content[] = '_new';
    $form_state->set('folder_type_allowed_content', $folder_type_allowed_content);
    $form_state->setRebuild();
  }

  /**
   * Submit callback to remove an allowed content item.
   */
  public function removeDefaultDisplayColorItemSubmit(array $form, FormStateInterface $form_state): void {
    $item_index = $form_state->getTriggeringElement()['#allowed_content_item_index'];
    $folder_type_allowed_content = (array) $form_state->get('folder_type_allowed_content');
    $item_id = $folder_type_allowed_content[$item_index];

    unset($folder_type_allowed_content[$item_index]);
    $form_state->set('folder_type_allowed_content', $folder_type_allowed_content);

    // Non-new values also need to be deleted from storage.
    if ($item_id !== '_new') {
      $delete_queue = (array) $form_state->get('delete_queue');
      $delete_queue[] = $item_id;
      $form_state->set('delete_queue', $delete_queue);
    }
    $form_state->setRebuild();
  }

  private function contentEntityTypeOptions(): array {
    $options = ['' => '- None -'];
    $entity_content_key = (string) $this->t('Content', [], ['context' => 'Entity type group']);
    $labels = $this->entityTypeRepository->getEntityTypeLabels(TRUE)[$entity_content_key];
    $content_entity_types_with_integer = array_filter(array_keys($labels), function ($entity_type_id) {
      return DynamicEntityReferenceItem::entityHasIntegerId($entity_type_id) && $entity_type_id !== 'folder';
    });
    return $options + array_intersect_key($labels, array_flip($content_entity_types_with_integer));
  }

  private function contentEntityTypeBundleOptions($entity_type) {
    if (!$entity_type) {
      return [];
    }
    return array_map(function($bundle_info) {
      return $bundle_info['label'];
    }, $this->entityTypeBundleInfo->getBundleInfo($entity_type));
  }

}
