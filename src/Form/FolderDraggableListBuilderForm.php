<?php

namespace Drupal\folder\Form;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\folder\Entity\FolderInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FolderDraggableListBuilderForm extends EntityListBuilder implements FormInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected PagerManagerInterface $pagerManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  protected UserInterface $user;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  protected $contentInSidebar;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->limit = 100;
    $instance->formBuilder = $container->get('form_builder');
    $instance->routeMatch = $container->get('current_route_match');
    $instance->pagerManager = $container->get('pager.manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->renderer = $container->get('renderer');
    $instance->entityRepository = $container->get('entity.repository');
    $user = $instance->routeMatch->getParameter('user');
    $instance->user = is_numeric($user) ? User::load($user) : $user;
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'folder_draggable_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    return $this->user ? $this->storage->loadMultipleByUser($this->user) : [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getTitle() {
    return $this->t('@user folders', [
      '@user' => $this->user->getDisplayName(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(array $build_header_metadata = []) {
    $no_pending_folder_ids = !empty($build_header_metadata['no_pending_folder_ids']);
    $update_tree_access = $build_header_metadata['update_tree_access'] ?? $this->getUpdateTreeAccessResult($no_pending_folder_ids);
    $header = [
      'folder' => $this->t('Name'),
      'weight' => $update_tree_access->isAllowed()  ? $this->t('Weight') : NULL,
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity, array $build_row_metadata = []) {
    /** @var \Drupal\folder\Entity\FolderInterface $entity */
    $no_pending_folder_ids = !empty($build_row_metadata['no_pending_folder_ids']);
    $update_tree_access = $build_row_metadata['update_tree_access'] ?? $this->getUpdateTreeAccessResult($no_pending_folder_ids);
    $tree_count = $build_row_metadata['tree_count'] ?? 0;
    $delta = $build_row_metadata['delta'] ?? 0;
    $parent_fields = $build_row_metadata['parent_fields'] ?? FALSE;
    $row_position = $build_row_metadata['row_position'] ?? 0;
    $back_step = $build_row_metadata['back_step'] ?? 0;
    $forward_step = $build_row_metadata['forward_step'] ?? 0;
    $page_entries = $build_row_metadata['page_entries'] ?? 0;
    $errors = $build_row_metadata['errors'] ?? [];
    $row_key = $build_row_metadata['row_key'] ?? $entity->id();

    $row = ['#folder' => $entity];

    $indentation = [];
    if (isset($entity->depth) && $entity->depth > 0) {
      $indentation = [
        '#theme' => 'indentation',
        '#size' => $entity->depth,
      ];
    }

    $row['folder']['data'] = [
      '#prefix' => !empty($indentation) ? $this->renderer->render($indentation) : '',
      '#type' => 'link',
      '#title' => $entity->label(),
      '#url' => $this->getViewFolderEntityUrl($entity),
    ];

    // Add a special class for folders with pending revision, so we can
    //  highlight them in the form.
    $row['#attributes']['class'] = [];
    if (!empty($build_row_metadata['in_pending_folder_ids'])) {
      $row['#attributes']['class'][] = 'color-warning';
      $row['#attributes']['class'][] = 'folder--pending-revision';
    }

    if ($parent_fields) {
      $row['#attributes']['class'][] = 'draggable';
      $row['folder']['fid'] = [
        '#type' => 'hidden',
        '#value' => $entity->id(),
        '#attributes' => [
          'class' => ['folder-id'],
        ],
      ];
      $row['folder']['parent'] = [
        '#type' => 'hidden',
        // Yes, default_value on a hidden. It needs to be changeable by
        // javascript.
        '#default_value' => $entity->getParentId(),
        '#attributes' => [
          'class' => ['folder-parent'],
        ],
      ];
      $row['folder']['depth'] = [
        '#type' => 'hidden',
        // Same as above, the depth is modified by javascript, so it's a
        // default_value.
        '#default_value' => $entity->depth,
        '#attributes' => [
          'class' => ['folder-depth'],
        ],
      ];
    }
    $update_access = $entity->access('update', NULL, TRUE);
    $update_tree_access = $update_tree_access->andIf($update_access);

    if ($update_tree_access->isAllowed()) {
      $weight = $entity->getWeight();
      // Override default values to markup elements.
      $row['#weight'] = $weight;
      $row['weight'] = [
        '#type' => 'weight',
        '#delta' => $delta,
        '#title' => t('Weight for @title', ['@title' => $entity->label()]),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        '#attributes' => ['class' => ['folder-weight']],
      ];
    }
    else {
      $row['weight'] = NULL;
    }

    // Add classes that mark which folders belong to previous and next pages.
    if ($row_position < $back_step || $row_position >= $page_entries - $forward_step) {
      $row['#attributes']['class'][] = 'folder-preview';
    }

    if ($row_position !== 0 && $row_position !== $tree_count - 1) {
      if ($row_position == $back_step - 1 || $row_position == $page_entries - $forward_step - 1) {
        $row['#attributes']['class'][] = 'folder-divider-top';
      }
      elseif ($row_position == $back_step || $row_position == $page_entries - $forward_step) {
        $row['#attributes']['class'][] = 'folder-divider-bottom';
      }
    }

    // Add an error class if this row contains a form error.
    foreach ($errors as $error_key => $error) {
      if (str_starts_with($error_key, $row_key)) {
        $row['#attributes']['class'][] = 'error';
      }
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = [
      'view' => [
        'title' => $this->t('View content'),
        'weight' => 1,
        'url' => $this->getViewFolderEntityUrl($entity),
      ],
    ] + parent::getDefaultOperations($entity);
    if (isset($operations['delete']['url']) && $operations['delete']['url'] instanceof Url) {
      $operations['delete']['url']->setOption('attributes', [
        'class' => ['use-ajax'],
        'data-dialog-options' => '{"width":"400"}',
        'data-dialog-type' => 'modal',
      ]);
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return $this->formBuilder->getForm($this);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $page = $this->pagerManager->findPage();
    // Number of folders per page.
    $page_increment = 100;
    // Elements shown on this page.
    $page_entries = 0;
    // Elements at the root level before this page.
    $before_entries = 0;
    // Elements at the root level after this page.
    $after_entries = 0;
    // Elements at the root level on this page.
    $root_entries = 0;
    $back_step = NULL;
    $forward_step = 0;

    // An array of the pages to be displayed on this page.
    $current_page = [];

    $delta = 0;
    $folder_deltas = [];
    $tree = $this->storage->loadTree('folder', 0, NULL, TRUE, $this->user->id());
    $tree_index = 0;
    $complete_tree = NULL;
    do {
      // In case, this tree is completely empty.
      if (empty($tree[$tree_index])) {
        break;
      }
      $delta++;
      // Count entries before the current page.
      if ($page && ($page * $page_increment) > $before_entries && !isset($back_step)) {
        $before_entries++;
        continue;
      }
      // Count entries after the current page.
      elseif ($page_entries > $page_increment && isset($complete_tree)) {
        $after_entries++;
        continue;
      }

      // Do not let a folder start the page that is not at the root.
      $folder = $tree[$tree_index];
      if (isset($folder->depth) && ($folder->depth > 0) && !isset($back_step)) {
        $back_step = 0;
        while ($p_folder = $tree[--$tree_index]) {
          $before_entries--;
          $back_step++;
          if ($p_folder->depth == 0) {
            $tree_index--;
            // Jump back to the start of the root level parent.
            continue 2;
          }
        }
      }
      $back_step = $back_step ?? 0;

      // Continue rendering the tree until we reach a new root item.
      if ($page_entries >= $page_increment + $back_step + 1 && $folder->depth == 0 && $root_entries > 1) {
        $complete_tree = TRUE;
        // This new item at the root level is the first item on the next page.
        $after_entries++;
        continue;
      }
      if ($page_entries >= $page_increment + $back_step) {
        $forward_step++;
      }

      // Finally, if we've gotten down this far, we're rendering a folder on
      // this page.
      $page_entries++;
      $folder_deltas[$folder->id()] = isset($folder_deltas[$folder->id()]) ? $folder_deltas[$folder->id()] + 1 : 0;
      $key = 'fid:' . $folder->id() . ':' . $folder_deltas[$folder->id()];

      // Keep track of the first folder displayed on this page.
      if ($page_entries == 1) {
        $form['#first_fid'] = $folder->id();
      }
      // Keep a variable to make sure at least 2 root elements are displayed.
      if ($folder->parents[0] == 0) {
        $root_entries++;
      }
      $current_page[$key] = $folder;
    } while (isset($tree[++$tree_index]));

    // Because we didn't use a pager query, set the necessary pager variables.
    $total_entries = $before_entries + $page_entries + $after_entries;
    $this->pagerManager->createPager($total_entries, $page_increment);

    // If this form was already submitted once, it's probably hit a validation
    // error. Ensure the form is rebuilt in the same order as the user
    // submitted.
    $user_input = $form_state->getUserInput();
    if (!empty($user_input['folders'])) {
      // Get the POST order.
      $order = array_flip(array_keys($user_input['folders']));
      // Update our form with the new order.
      $current_page = array_merge($order, $current_page);
      foreach ($current_page as $key => $folder) {
        // Verify this is a folder for the current page and set at the current
        // depth.
        if (is_array($user_input['folders'][$key]) && is_numeric($user_input['folders'][$key]['folder']['fid'])) {
          $current_page[$key]->depth = $user_input['folders'][$key]['folder']['depth'];
        }
        else {
          unset($current_page[$key]);
        }
      }
    }

    $help_message = $this->getHelpMessage();
    // Get the IDs of the folders edited on the current page which have
    // pending revisions.
    $edited_folder_ids = array_map(function ($item) {
      return $item->id();
    }, $current_page);
    $pending_folder_ids = array_intersect($this->storage->getTermIdsWithPendingRevisions(), $edited_folder_ids);
    if ($pending_folder_ids) {
      $help_message = $this->formatPlural(
        count($pending_folder_ids),
        'There is 1 folder with pending revisions. Drag and drop of folders with pending revisions is not supported, but you can re-enable drag-and-drop support by getting each folder to a published state.',
        'There are @count folders with pending revisions. Drag and drop of folders with pending revisions is not supported, but you can re-enable drag-and-drop support by getting each folder to a published state.'
      );
    }

    $update_tree_access = $this->getUpdateTreeAccessResult(empty($pending_folder_ids));

    $form['help'] = [
      '#type' => 'container',
      'message' => ['#markup' => $help_message],
    ];
    if (!$update_tree_access->isAllowed()) {
      $form['help']['#attributes']['class'] = ['messages', 'messages--warning'];
    }

    $errors = $form_state->getErrors();
    $row_position = 0;
    // Build the actual form.
    $create_access = $this->getCreateAccessResult();
    if ($create_access->isAllowed()) {
      $empty = $this->t('No folders available. <a href=":link">Add folder</a>.', [':link' => Url::fromRoute('entity.folder.add_form', [
        'folder_type' => 'folder',
        'user' => $this->user->id(),
      ])->toString()]);
    }
    else {
      $empty = $this->t('No folders available.');
    }

    $content_in_teaser = $this->contentInTeaser();
    $wrapper_prefix = $content_in_teaser ? '<div class="folders--table-content-wrapper"><div class="folders--table-wrapper">' : '';

    $form['folders'] = [
      '#type' => 'table',
      '#prefix' => $wrapper_prefix,
      '#suffix' => $wrapper_prefix ? '</div>' : '',
      '#empty' => $empty,
      '#title' => $this->getTitle(),
      '#header' => $this->buildHeader(['update_tree_access' => $update_tree_access]),
      '#attributes' => [
        'id' => 'folders_list_table',
      ],
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];
    $this->renderer->addCacheableDependency($form['folders'], $create_access);

    $tree_count = count($tree);
    $parent_fields = $update_tree_access->isAllowed() && $tree_count > 1;
    foreach ($current_page as $key => $folder) {
      $folder = $this->entityRepository->getTranslationFromContext($folder);
      $build_row_metadata = [
        'in_pending_folder_ids' => in_array($folder->id(), $pending_folder_ids),
        'update_tree_access' => $update_tree_access,
        'tree_count' => $tree_count,
        'delta' => $delta,
        'parent_fields' => $parent_fields,
        'row_position' => $row_position,
        'back_step' => $back_step,
        'page_entries' => $page_entries,
        'forward_step' => $forward_step,
        'errors' => $errors,
        'row_key' => $key,
      ];
      $form['folders'][$key] = $this->buildRow($folder, $build_row_metadata);
      $row_position++;
    }

    $this->renderer->addCacheableDependency($form['folders'], $update_tree_access);
    if ($update_tree_access->isAllowed()) {
      if ($parent_fields) {
        $form['folders']['#tabledrag'][] = [
          'action' => 'match',
          'relationship' => 'parent',
          'group' => 'folder-parent',
          'subgroup' => 'folder-parent',
          'source' => 'folder-id',
          'hidden' => FALSE,
        ];
        $form['folders']['#tabledrag'][] = [
          'action' => 'depth',
          'relationship' => 'group',
          'group' => 'folder-depth',
          'hidden' => FALSE,
        ];
        $form['folders']['#attached']['library'][] = 'folder/list';
        $form['folders']['#attached']['drupalSettings']['folder'] = [
          'backStep' => $back_step,
          'forwardStep' => $forward_step,
        ];
      }
      $form['folders']['#tabledrag'][] = [
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'folder-weight',
      ];
    }

    $form['folder'] = [
      '#type' => 'container',
      '#access' => $content_in_teaser && count($current_page) >= 1,
      '#suffix' => '</div>',
      '#attached' => ['library' => [
        'core/drupal.ajax',
        'folder/teaser'
      ]],
      '#attributes' => ['class' => ['folders--folder-content-wrapper']],
      'content' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'data-folders-folder-container' => TRUE,
        ],
        '#value' => '<p>' . $this->t('Click on "view content" operations or the folder title to view it\'s content here.') . '</p>',
      ],
    ];

    if ($parent_fields) {
      $form = $this->actions($form, $form_state);
    }

    $form['pager_pager'] = ['#type' => 'pager'];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Sort folder order based on weight.
    uasort($form_state->getValue('folders'), ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);

    $folder_type_id = 'folder';
    $changed_folders = [];
    $tree = $this->storage->loadTree($folder_type_id, 0, NULL, TRUE, $this->user->id());

    if (empty($tree)) {
      return;
    }

    // Build a list of all folders that need to be updated on previous pages.
    $weight = 0;
    /** @var \Drupal\folder\Entity\FolderInterface $folder */
    $folder = $tree[0];
    $weight_update_message = $this->t('Folder changed weight.');
    while ($folder->id() != $form['#first_fid']) {
      if ($folder->parents[0] == 0 && $folder->getWeight() != $weight) {
        $folder->setWeight($weight);
        $folder->setRevisionLogMessage((string)$weight_update_message);
        $changed_folders[$folder->id()] = $folder;
      }
      $weight++;
      $folder = $tree[$weight];
    }

    // Renumber the current page weights and assign any new parents.
    $level_weights = [];
    foreach ($form_state->getValue('folders') as $fid => $values) {
      if (!isset($form['folders'][$fid]['#folder'])) {
        continue;
      }

      /** @var \Drupal\folder\Entity\FolderInterface $folder */
      $folder = $form['folders'][$fid]['#folder'];
      // Give folders at the root level a weight in sequence with folders on
      // previous pages.
      if ($values['folder']['parent'] == 0 && $folder->getWeight() != $weight) {
        $folder->setWeight($weight);
        if ($folder->getRevisionLogMessage() !== (string)$weight_update_message) {
          $folder->setRevisionLogMessage((string)$weight_update_message);
        }
        $changed_folders[$folder->id()] = $folder;
      }
      // Folders not at the root level can safely start from 0 because
      // they're all on this page.
      elseif ($values['folder']['parent'] > 0) {
        $level_weights[$values['folder']['parent']] = isset($level_weights[$values['folder']['parent']]) ? $level_weights[$values['folder']['parent']] + 1 : 0;
        if ($level_weights[$values['folder']['parent']] != $folder->getWeight()) {
          $folder->setWeight($level_weights[$values['folder']['parent']]);
          $changed_folders[$folder->id()] = $folder;
        }
      }
      // Update any changed parents.
      if ($values['folder']['parent'] != $folder->parents[0]) {
        $folder->parent->target_id = $values['folder']['parent'];
        $log_msg = $folder->getRevisionLogMessage() ?? '';
        $separator = $log_msg ? ' -- ' : '';
        $parent_update_msg = (string) $this->t('The folder parent have been updated.');
        $log_msg .= $log_msg . $separator . $parent_update_msg;
        $folder->setRevisionLogMessage($log_msg);
        $changed_folders[$folder->id()] = $folder;
      }
      $weight++;
    }

    // Build a list of all folders that need to be updated on the following
    // pages.
    for ($weight; $weight < count($tree); $weight++) {
      $folder = $tree[$weight];
      if ($folder->parents[0] == 0 && $folder->getWeight() != $weight) {
        $folder->parent->target_id = $folder->parents[0];
        $folder->setWeight($weight);
        $log_msg = $folder->getRevisionLogMessage() ?? '';
        $separator = $log_msg ? ' -- ' : '';
        if (!str_contains($log_msg, (string)$weight_update_message)) {
          $log_msg .= $log_msg . $separator . (string)$weight_update_message;
          $folder->setRevisionLogMessage($log_msg);
        }
        $changed_folders[$folder->id()] = $folder;
      }
    }

    if (empty($changed_folders)) {
      return;
    }

    $pending_folder_ids = $this->storage->getTermIdsWithPendingRevisions();

    // Force a form rebuild if any of the changed folders has a pending
    // revision.
    if (array_intersect_key(array_flip($pending_folder_ids), $changed_folders)) {
      $this->messenger()->addError($this->t('The folders with updated parents have been modified by another user, the changes could not be saved.'));
      $form_state->setRebuild();
      return;
    }

    $has_error = FALSE;
    // Save all updated folders.
    foreach ($changed_folders as $folder) {
      /** @var \Drupal\folder\Entity\FolderInterface $folder */
      $violations = $folder->validate();
      if ($violations->count()) {
        foreach ($violations as $violation) {
          $this->messenger()->addError($violation->getMessage());
          $has_error = TRUE;
        }
        continue;
      }

      $folder->setRevisionUser($this->user);
      $folder->save();
    }

    if ($has_error) {
      $this->messenger()->addWarning($this->t('The folder options have not been updated due to the errors reported above.'));
    }
    else {
      $this->messenger()->addStatus($this->t('The folder options have been saved.'));
    }
  }

  /**
   * Redirects to a confirmation form for the reset action.
   */
  public function submitReset(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirectUrl(Url::fromRoute('entity.folder.collection', [
      'user' => $this->user->id(),
    ]));
  }

  protected function actions(array $form, FormStateInterface $form_state): array {
    $form['actions'] = ['#type' => 'actions', '#tree' => FALSE];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    $form['actions']['reset_alphabetical'] = [
      '#type' => 'submit',
      '#submit' => ['::submitReset'],
      '#value' => $this->t('Reset to alphabetical'),
    ];
    return $form;
  }

  protected function getHelpMessage(): MarkupInterface {
    $is_admin = $this->currentUser()->hasPermission('administer folders');
    $is_folders_owner = $this->user->id() === $this->currentUser()->id();
    if ($is_admin || $is_folders_owner) {
      return $this->t('Folders are grouped under parent folder. You can reorganize the folders using their drag-and-drop handles.');
    }
    return $this->t('Folders are grouped under parent folder.');
  }

  /**
   * Gets the current user.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   The current user.
   */
  protected function currentUser() {
    return \Drupal::currentUser();
  }

  protected function getCreateAccessResult(): AccessResultInterface {
    $access_control_handler = $this->entityTypeManager->getAccessControlHandler('folder');
    return $access_control_handler->createAccess('folder', NULL, [], TRUE);
  }

  protected function getUpdateTreeAccessResult(bool $no_pending_folder_ids): AccessResultInterface {
    return AccessResult::allowedIf($no_pending_folder_ids);
  }

  /**
   * Retrieves a configuration object.
   *
   * @param string $name
   *   The name of the configuration objects to retrieve.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   An immutable configuration object.
   */
  protected function config(string $name) {
    return \Drupal::config($name);
  }

  protected function contentInTeaser(): bool {
    if (!isset($this->contentInSidebar)) {
      $config = $this->config('folder.configurations');
      $this->contentInSidebar = !empty($config->get('view_content_in_teaser'));
    }
    return $this->contentInSidebar;
  }

  protected function getViewFolderEntityUrl(FolderInterface $folder): Url {
    if ($this->contentInTeaser()) {
      $parameters = [
        'user' => $this->user->id(),
        'folder' => $folder->id(),
        'nojs' => 'nojs',
      ];
      return Url::fromRoute('entity.folder.teaser', $parameters, [
        'attributes' => ['class' => 'use-ajax'],
      ]);
    }
    return $folder->toUrl();
  }

}
