<?php

namespace Drupal\folder;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\UncacheableEntityPermissionProvider;
use Drupal\folder\Entity\FolderType;

/**
 * Permission provider callback for the folder module.
 */
class FolderPermissionsProvider extends UncacheableEntityPermissionProvider {

  /**
   * {@inheritdoc}
   */
  protected function buildBundlePermissions(EntityTypeInterface $entity_type) {
    $permissions = parent::buildBundlePermissions($entity_type);

    // Adding bundles/folder types as a dependency to related bundle permission.
    $entity_type_id = $entity_type->id();
    $folder_types = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
    $folder_type_entity_handlers = $folder_types ? FolderType::loadMultiple(array_keys($folder_types)) : [];
    foreach ($folder_type_entity_handlers as $folder_type_id => $folder_type) {
      $folder_type_permissions = array_filter($permissions, function($permission_key) use ($folder_type_id, $entity_type_id) {
        return str_ends_with($permission_key, "{$folder_type_id} {$entity_type_id}");
      }, ARRAY_FILTER_USE_KEY);

      foreach ($folder_type_permissions as $permission_key => $permission) {
        $permissions[$permission_key]['dependencies'][$folder_type->getConfigDependencyKey()][] = $folder_type->getConfigDependencyName();
      }
    }
    return $permissions;
  }

}

