<?php

namespace Drupal\folder\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\Routing\DefaultHtmlRouteProvider;
use Drupal\folder\Controller\FolderController;
use Drupal\folder\Form\AddContentToFolderForm;
use Symfony\Component\Routing\Route;

/**
 * Provides HTML routes for folders.
 */
class FolderRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();
    if ($add_content_route = $this->getAddContentRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.add_content_form", $add_content_route);
    }

    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAddPageRoute(EntityTypeInterface $entity_type) {
    if ($route = parent::getAddPageRoute($entity_type)) {
      $route->setDefault('_controller', FolderController::class . '::addPage');
      $parameters = $route->getOption('parameters');
      $parameters['user'] = ['type' => 'entity:user'];
      $route->setOption('parameters', $parameters);
      return $route;
    }
    return parent::getAddPageRoute($entity_type);
  }

  protected function getAddContentRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('add-content-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('add-content-form'));
      $route
        ->setDefaults([
          '_form' => AddContentToFolderForm::class . '::form',
          '_title_callback' => AddContentToFolderForm::class . '::title',
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.collection")
        ->setOption('parameters', [
          'user' => ['type' => 'entity:user'],
        ]);
    }
    return NULL;
  }

}
