<?php

namespace Drupal\folder\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\StatusItem;

/**
 * Defines the folder entity.
 *
 * @ContentEntityType(
 *   id = "folder",
 *   label = @Translation("Folder"),
 *   label_collection = @Translation("Folders"),
 *   label_singular = @Translation("Folder"),
 *   label_plural = @Translation("Folders"),
 *   label_count = @PluralTranslation(
 *     singular = "@count folder",
 *     plural = "@count folders",
 *   ),
 *   bundle_label = @Translation("Folder type"),
 *   handlers = {
 *     "storage" = "Drupal\folder\FolderStorage",
 *     "storage_schema" = "Drupal\folder\FolderStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\folder\Form\FolderDraggableListBuilderForm",
 *     "access" = "\Drupal\entity\UncacheableEntityAccessControlHandler",
 *     "permission_provider" = "\Drupal\folder\FolderPermissionsProvider",
 *     "views_data" = "\Drupal\folder\FolderViewsData",
 *     "translation" = "Drupal\folder\FolderTranslationHandler",
 *     "form" = {
 *       "default" = "Drupal\folder\Form\FolderForm",
 *       "delete" = "Drupal\folder\Form\FolderDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\folder\Routing\FolderRouteProvider",
 *       "revision" = "\Drupal\entity\Routing\RevisionRouteProvider",
 *       "delete-multiple" = "\Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *     "local_action_provider" = {
 *       "collection" = "\Drupal\folder\Menu\FolderCollectionLocalActionProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "\Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *   },
 *   base_table = "folder_data",
 *   data_table = "folder_field_data",
 *   revision_table = "folder_revision",
 *   revision_data_table = "folder_field_revision",
 *   translatable = TRUE,
 *   revisionable = TRUE,
 *   admin_permission = "administer folder",
 *   permission_granularity = "bundle",
 *   entity_keys = {
 *     "id" = "fid",
 *     "bundle" = "type",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   bundle_entity_type = "folder_type",
 *   field_ui_base_route = "entity.folder_type.edit_form",
 *   common_reference_target = TRUE,
 *   links = {
 *     "canonical" = "/user/{user}/folders/{folder}",
 *     "add-page" = "/user/{user}/folders/add",
 *     "add-form" = "/user/{user}/folders/add/{folder_type}",
 *     "edit-form" = "/user/{user}/folders/{folder}/edit",
 *     "collection" = "/user/{user}/folders",
 *     "delete-form" = "/user/{user}/folders/{folder}/delete",
 *     "delete-multiple-form" = "/user/{user}/folders/delete",
 *     "revision" = "/user/{user}/folders/{folder}/revisions/{folder_revision}/view",
 *     "revision-revert-form" = "/user/{user}/folders/{folder}/revisions/{folder_revision}/revert",
 *     "version-history" = "/user/{user}/folders/{folder}/revisions",
 *   },
 *   constraints = {
 *     "FolderHierarchy" = {},
 *     "FolderName" = {},
 *   },
 * )
 */
class Folder extends EditorialContentEntityBase implements FolderInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    $uri_route_parameters['user'] = $this->getOwnerId();
    $uri_route_parameters['folder_parent'] = $this->getParentId();
    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // See if any of the folder's children are about to become orphans.
    $orphans = [];
    /** @var \Drupal\folder\Entity\FolderInterface $folder */
    foreach ($entities as $fid => $folder) {
      if ($children = $storage->getChildren($folder)) {
        /** @var \Drupal\folder\Entity\FolderInterface $child */
        foreach ($children as $child) {
          $parent = $child->get('parent');
          // Update the child parents item list.
          $parent->filter(function ($item) use ($fid) {
            return $item->target_id != $fid;
          });

          // If the folder has multiple parents, we don't delete it.
          if ($parent->count()) {
            $child->save();
          }
          else {
            $orphans[] = $child;
          }
        }
      }
    }

    if (!empty($orphans)) {
      $storage->delete($orphans);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    // Folders with no parents are mandatory children of <root>.
    if (!$this->get('parent')->count()) {
      $this->parent->target_id = 0;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getParent(): ?FolderInterface {
    return $this->get('parent')->entity ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentId(): int {
    $parent = $this->getParent();
    return $parent ? $parent->id() : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function setParent(FolderInterface $folder) {
    $this->set('parent', $folder);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentId(int $fid) {
    $this->set('parent', $fid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormat() {
    return $this->get('description')->format;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormat($format) {
    $this->get('description')->format = $format;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->label();
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return (int) $this->get('weight')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  public function getContentItems(): array {
    return $this->get('content')->referencedEntities();
  }

  public function hasContentItems(): bool {
    return !$this->get('content')->isEmpty();
  }

  public function addContentItem(ContentEntityInterface $content_item, bool $skip_check = FALSE) {
    if ($skip_check || !$this->hasContentItem($content_item)) {
      $this->get('content')->appendItem($content_item);
    }
    return $this;
  }

  public function removeContentItem(ContentEntityInterface $content_item) {
    $index = $this->getContentItemIndex($content_item);
    if ($index !== FALSE) {
      $this->get('content')->offsetUnset($index);
    }
    return $this;
  }

  public function hasContentItem(ContentEntityInterface $content_item): bool {
    return $this->getContentItemIndex($content_item) !== FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['status']->getItemDefinition()->setClass(StatusItem::class);
    $fields['status']
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 100,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['fid']->setLabel(t('Folder ID'))
      ->setDescription(t('The folder ID.'));

    $fields['uid']->setLabel(t('Folder Owner'))
      ->setDescription(t('The user who owns this folder.'))
      ->addConstraint('FolderHasSameOwnerAsParent', []);

    $fields['uuid']->setDescription(t('The folder UUID.'));

    $fields['type']->setLabel(t('Folder type'))
      ->setDescription(t('The folder type to which the folder is assigned.'));

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of this folder in relation to other folders.'))
      ->setDefaultValue(0);

    $fields['parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Folder Parent'))
      ->setDescription(t('The parent folder of this folder.'))
      ->setCardinality(1)
      ->setSetting('target_type', 'folder')
      ->setDisplayConfigurable('view', TRUE);

    $fields['content'] = BaseFieldDefinition::create('dynamic_entity_reference')
      ->setLabel(t('Content items'))
      ->setDescription(t('The folder content items.'))
      ->setRequired(FALSE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('exclude_entity_types', TRUE)
      ->setSetting('entity_type_ids', ['folder'])
      ->setDisplayOptions('form', [
        'type' => 'dynamic_entity_reference_default',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the folder was last updated.'))
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE);

    $fields['revision_log_message']->setDisplayOptions('form', [
      'region' => 'hidden',
    ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    // Only folders in the same bundle can be a parent.
    $fields['parent'] = clone $base_field_definitions['parent'];
    $fields['parent']->setSetting('handler_settings', ['target_bundles' => [$bundle => $bundle]]);

    // Update the content dynamic reference field settings based on the bundle
    // config.
    /** @var \Drupal\folder\Entity\FolderType $folder_type */
    $folder_type = FolderType::load($bundle);
    $allowed_content = $folder_type->getAllowedContent();
    if ($allowed_content) {
      $fields['content'] = clone $base_field_definitions['content'];
      $entity_types_settings = [];
      foreach ($allowed_content as $setting) {
        $entity_types_settings[$setting['entity_type_id']] = [$setting['bundle'] => $setting['bundle']];
      }
      $fields['content']->setSetting('exclude_entity_types', FALSE)
        ->setSetting('entity_type_ids', array_keys($entity_types_settings));
      foreach ($entity_types_settings as $entity_type_id => $bundles) {
        $fields['content']->setSetting($entity_type_id, [
          'handler' => "default:{$entity_type_id}",
          'handler_settings' => ['target_bundles' => $bundles],
        ]);
      }
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaultEntityOwner() {
    $route_match = \Drupal::routeMatch();
    $user = $route_match->getParameter('user');
    if ($user instanceof AccountInterface) {
      return $user->id();
    }
    elseif (is_numeric($user)) {
      return $user;
    }
    return \Drupal::currentUser()->id();
  }

  public static function supportedEntityTypes(): array {
    return [
      'taxonomy_term',
      'node',
      'media',
      'file',
      'webform_submission',
      'group',
    ];
  }

  /**
   * Gets the index of the given content item.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $content_item
   *   The content item.
   *
   * @return int|bool
   *   The index of the given content item, or FALSE if not found.
   */
  protected function getContentItemIndex(ContentEntityInterface $content_item): int|bool {
    $content_item_ids = [];
    $content_item_id = $content_item->getEntityTypeId() . '__' . $content_item->id();
    foreach ($this->getContentItems() as $entity_item) {
      $content_item_ids[] = $entity_item->getEntityTypeId() . '__' . $entity_item->id();
    }
    return $content_item_ids ? array_search($content_item_id, $content_item_ids) : FALSE;
  }

}
