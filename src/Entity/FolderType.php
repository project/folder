<?php

namespace Drupal\folder\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Defines the folder type entity.
 *
 * @ConfigEntityType(
 *   id = "folder_type",
 *   label = @Translation("Folder type"),
 *   label_singular = @Translation("folder type"),
 *   label_plural = @Translation("folder types"),
 *   label_collection = @Translation("Folder type"),
 *   label_count = @PluralTranslation(
 *     singular = "@count folder type",
 *     plural = "@count folder types"
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\folder\FolderTypeStorage",
 *     "list_builder" = "Drupal\folder\FolderTypeListBuilder",
 *     "access" = "Drupal\folder\FolderTypeAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\folder\Form\FolderTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *       "permissions" = "Drupal\user\Entity\EntityPermissionsRouteProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "local_action_provider" = {
 *       "collection" = "\Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *   },
 *   admin_permission = "administer folder",
 *   config_prefix = "type",
 *   bundle_of = "folder",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/folder-types/add",
 *     "edit-form" = "/admin/structure/folder-types/manage/{folder_type}",
 *     "delete-form" = "/admin/structure/folder-types/manage/{folder_type}/delete",
 *     "entity-permissions-form" = "/admin/structure/folder-types/manage/{folder_type}/permissions",
 *     "collection" = "/admin/structure/folder-types",
 *   },
 *   config_export = {
 *     "name",
 *     "id",
 *     "description",
 *     "weight",
 *     "allowed_content",
 *   }
 * )
 */
class FolderType extends Vocabulary implements FolderTypeInterface {

  /**
   * The folder type ID.
   *
   * @var string
   */
  protected $id;

  protected array $allowed_content = [];

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function get($property_name) {
    if ($property_name === 'vid') {
      $property_name = 'id';
    }
    return parent::get($property_name);
  }

  /**
   * {@inheritdoc}
   */
  public function set($property_name, $value) {
    if ($property_name === 'vid') {
      $property_name = 'id';
    }
    return parent::set($property_name, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowedContent(): array {
    return $this->allowed_content;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    $entity_type_manager = $this->entityTypeManager();
    foreach ($this->getAllowedContent() as $allowed_content) {
      $entity_type_id = $allowed_content['entity_type_id'];
      $entity_type = $entity_type_manager->getDefinition($entity_type_id);

      if ($entity_type_bundle_entity_type_id = $entity_type->getBundleEntityType()) {
        $bundle_storage = $entity_type_manager->getStorage($entity_type_bundle_entity_type_id);
        $bundle_id = $allowed_content['bundle'];
        /** @var \Drupal\Core\Config\Entity\ConfigEntityBundleBase $bundle */
        if ($bundle = $bundle_storage->load($bundle_id)) {
          $this->addDependency('config', $bundle->getConfigDependencyName());
        }
        else {
          $this->addDependency('module', $entity_type->getProvider());
        }
      }
      else {
        $this->addDependency('module', $entity_type->getProvider());
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);

    // Only load folders without a parent, child folders will get deleted too.
    $folder_storage = \Drupal::entityTypeManager()->getStorage('folder');
    $folders = $folder_storage->loadMultiple($storage->getToplevelFids(array_keys($entities)));
    $folder_storage->delete($folders);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Reset caches.
    $storage->resetCache(array_keys($entities));

    if (reset($entities)->isSyncing()) {
      return;
    }

    $folder_types = [];
    foreach ($entities as $type) {
      $folder_types[$type->id()] = $type->id();
    }
    // Load all folder module fields and delete those which use only this type.
    $field_storages = \Drupal::entityTypeManager()->getStorage('field_storage_config')->loadByProperties(['module' => 'folder']);
    foreach ($field_storages as $field_storage) {
      $modified_storage = FALSE;
      // Folder reference fields may reference folders from more than one type.
      foreach ($field_storage->getSetting('allowed_values') as $key => $allowed_value) {
        if (isset($folder_types[$allowed_value['type']])) {
          $allowed_values = $field_storage->getSetting('allowed_values');
          unset($allowed_values[$key]);
          $field_storage->setSetting('allowed_values', $allowed_values);
          $modified_storage = TRUE;
        }
      }
      if ($modified_storage) {
        $allowed_values = $field_storage->getSetting('allowed_values');
        if (empty($allowed_values)) {
          $field_storage->delete();
        }
        else {
          // Update the field definition with the new allowed values.
          $field_storage->save();
        }
      }
    }
  }

}
