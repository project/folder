<?php

namespace Drupal\folder\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\yearly_country_data\Entity\YearlyCountryDataItemInterface;

interface FolderInterface extends TermInterface, EntityOwnerInterface {

  /**
   * Gets the parent folder of the current folder.
   *
   * @return \Drupal\folder\Entity\FolderInterface|null
   *   The paren folder of the current folder.
   */
  public function getParent(): ?FolderInterface;

  /**
   * Gets the current folder parent id.
   *
   * @return int
   *   The current parent id or zero if none.
   */
  public function getParentId(): int;

  /**
   * Sets the folder parent's folder entity.
   *
   * @param \Drupal\folder\Entity\FolderInterface $folder
   *   The parent folder entity.
   *
   * @return $this
   */
  public function setParent(FolderInterface $folder);

  /**
   * Sets the folder parent's ID.
   *
   * @param int $fid
   *   The owner user id.
   *
   * @return $this
   */
  public function setParentId(int $fid);

  /**
   * Get content items.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The content items.
   */
  public function getContentItems(): array;

  public function hasContentItems(): bool;

  public function addContentItem(ContentEntityInterface $content_item, bool $skip_check = FALSE);

  public function removeContentItem(ContentEntityInterface $content_item);

  public function hasContentItem(ContentEntityInterface $content_item): bool;

}
