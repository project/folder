<?php

namespace Drupal\folder\Entity;

use Drupal\taxonomy\VocabularyInterface;

/**
 * Provides an interface defining a folder type entity.
 */
interface FolderTypeInterface extends VocabularyInterface {

  /**
   * Gets allowed content settings.
   *
   * @return array
   *   The allowed content settings.
   */
  public function getAllowedContent(): array;

}
