<?php

namespace Drupal\folder;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of folder type entities.
 *
 * @see \Drupal\folder\Entity\FolderType
 */
class FolderTypeListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  protected $entitiesKey = 'folder_types';

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->currentUser = $container->get('current_user');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'folder_types_overview';
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if (isset($operations['edit'])) {
      $operations['edit']['title'] = t('Edit folder type');
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = t('Folder type name');
    $header['description'] = t('Description');

    if ($this->currentUser->hasPermission('administer folder types') && !empty($this->weightKey)) {
      $header['weight'] = t('Weight');
    }

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['description']['data'] = ['#markup' => $entity->getDescription()];
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $entities = $this->load();
    // If there are no multiple folder types, disable dragging by unsetting the
    // weight key.
    if (count($entities) <= 1) {
      unset($this->weightKey);
    }
    $build = parent::render();

    // If the weight key was unset, then the table is in the 'table' key,
    // otherwise in folder types. The empty message is only needed if the table
    // is possibly empty, so there is no need to support the folder type key
    // here.
    if (isset($build['table'])) {
      $access_control_handler = $this->entityTypeManager->getAccessControlHandler('folder_type');
      $create_access = $access_control_handler->createAccess(NULL, NULL, [], TRUE);
      $this->renderer->addCacheableDependency($build['table'], $create_access);
      if ($create_access->isAllowed()) {
        $build['table']['#empty'] = t('No folder types available. <a href=":link">Add a folder type</a>.', [
          ':link' => Url::fromRoute('entity.folder_type.add_form')->toString(),
        ]);
      }
      else {
        $build['table']['#empty'] = t('No folder types available.');
      }
    }

    return $build;
  }

}
