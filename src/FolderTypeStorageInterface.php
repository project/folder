<?php

namespace Drupal\folder;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * Defines an interface for folder type entity storage classes.
 */
interface FolderTypeStorageInterface extends ConfigEntityStorageInterface {

  /**
   * Gets top-level folder IDs of folder types.
   *
   * @param array $folder_type_ids
   *   Array of folder type IDs.
   * @param int $uid
   *   The user id.
   *
   * @return array
   *   Array of top-level folder IDs.
   */
  public function getToplevelFids(array $folder_type_ids, int $uid = 0): array;

}
