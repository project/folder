<?php

namespace Drupal\folder;

use Drupal\entity\EntityViewsData;

class FolderViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Link to self through left.parent = right.tid (going down in depth).
    $data['folder__parent']['table']['join']['folder__parent'] = [
      'left_field' => 'entity_id',
      'field' => 'parent_target_id',
    ];

    $data['folder__parent']['parent_target_id']['help'] = $this->t('The parent folder of the folder.');
    $data['folder__parent']['parent_target_id']['relationship']['label'] = $this->t('Parent');
    $data['folder__parent']['parent_target_id']['argument']['id'] = 'folder';

    return $data;
  }

}
