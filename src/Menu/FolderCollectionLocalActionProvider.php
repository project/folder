<?php

namespace Drupal\folder\Menu;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Url;
use Drupal\entity\Menu\EntityCollectionLocalActionProvider;
use Drupal\user\Entity\User;

class FolderCollectionLocalActionProvider extends EntityCollectionLocalActionProvider {

  /**
   * {@inheritdoc}
   */
  public function buildLocalActions(EntityTypeInterface $entity_type) {
    $actions = parent::buildLocalActions($entity_type);
    if ($actions) {
      $user = \Drupal::routeMatch()->getParameter('user');
      if (is_numeric($user)) {
        $user = User::load($user);
      }

      if (!$user) {
        // Let remove the destination redirect from action query links.
        foreach ($actions as &$action) {
          if (isset($action['options']['query']['destination'])) {
            unset($action['options']['query']['destination']);
          }
        }
      }
      else {
        $parameters = ['user' => $user->id()];
        foreach ($actions as &$action) {
//          $url = Url::fromRoute('entity.folder.collection', $parameters)->toString(TRUE);
//          $action['options']['query']['destination'] = $url instanceof GeneratedUrl ? $url->getGeneratedUrl() : $url;
          if (isset($action['options']['query']['destination'])) {
            unset($action['options']['query']['destination']);
          }
        }
      }
    }
    return $actions;
  }

}
