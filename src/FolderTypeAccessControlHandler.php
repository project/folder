<?php

namespace Drupal\folder;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the folder type entity type.
 *
 * @see \Drupal\folder\Entity\FolderType
 */
class FolderTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'access folder_type overview':
      case 'view':
        return AccessResult::allowedIfHasPermissions($account, ['access folder_type overview', 'administer folder'], 'OR');

      default:
        return parent::checkAccess($entity, $operation, $account);
    }
  }

}
