<?php

namespace Drupal\folder;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

class FolderStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    if ($data_table = $this->storage->getDataTable()) {
      $schema[$data_table]['indexes'] += [
        'folder__tree' => ['type', 'weight', 'name'],
      ];
    }

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping) {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);

    if ($table_name !== 'folder_field_data') {
      return $schema;
    }

    // Remove unneeded indexes.
    unset($schema['indexes']['folder_field__type__target_id']);
    unset($schema['indexes']['folder_field__description__format']);

    $field_name = $storage_definition->getName();
    switch ($field_name) {
      case 'weight':
        // Improves the performance of the folder__tree index defined in
        // getEntitySchema().
        $schema['fields'][$field_name]['not null'] = TRUE;
        break;
      case 'name':
        $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
        break;
    }

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDedicatedTableSchema(FieldStorageDefinitionInterface $storage_definition, ContentEntityTypeInterface $entity_type = NULL) {
    $dedicated_table_schema = parent::getDedicatedTableSchema($storage_definition, $entity_type);
    if ($storage_definition->getName() !== 'parent') {
      return $dedicated_table_schema;
    }

    // Add an index on 'bundle', 'delta' and 'parent_target_id' columns to
    // increase the performance of the query from
    // \Drupal\folder\FolderStorage::getFolderTypeHierarchyType().
    /** @var \Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
    $table_mapping = $this->storage->getTableMapping();
    $dedicated_table_name = $table_mapping->getDedicatedDataTableName($storage_definition);

    unset($dedicated_table_schema[$dedicated_table_name]['indexes']['bundle']);
    $dedicated_table_schema[$dedicated_table_name]['indexes']['bundle_delta_target_id'] = [
      'bundle',
      'delta',
      $table_mapping->getFieldColumnName($storage_definition, 'target_id'),
    ];

    return $dedicated_table_schema;
  }

}
