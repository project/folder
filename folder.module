<?php

/**
 * @file
 * Implements hooks related to the folder module.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\folder\Entity\Folder;
use Drupal\folder\Entity\FolderInterface;
use Drupal\user\Entity\User;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_theme().
 */
function folder_theme($existing, $type, $theme, $path) {
  return [
    'folder' => [
      'render element' => 'elements',
    ],
    'folder_add_content_link' => [
      'variables' => [
        'entity' => NULL,
        'user' => NULL,
        'load_content_folder_entities' => FALSE,
      ],
    ],
  ];
}

/**
 * Implements hook_views_pre_execute().
 */
function folder_views_pre_execute(ViewExecutable $view) {
  // Adding a fix for the dynamic entity reference views relationship for the
  // folder table.
  // @todo maybe do a views data alter.
  $query = $view->build_info['query'];
  if (!$query instanceof SelectInterface) {
    return;
  }

  foreach ($query->getTables() as &$table_info) {
    if ($table_info['table'] === 'folder_data__content') {
      $table_info['table'] = 'folder__content';
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK() for folder.
 */
function folder_theme_suggestions_folder(array $variables) {
  $folder = $variables['elements']['#folder'];
  assert($folder instanceof FolderInterface);

  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  return [
    'folder__' . $sanitized_view_mode,
    'folder__' . $folder->bundle(),
    'folder__' . $folder->bundle() . '__' . $sanitized_view_mode,
  ];
}

/**
 * Prepares variables for the folder entity.
 *
 * @param array $variables
 *   The variables.
 */
function template_preprocess_folder(array &$variables) {
  $folder = $variables['elements']['#folder'];
  $variables['folder'] = $folder;
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  $variables['label'] = $folder->label();

  $variables['attributes']['class'][] = 'folder';
  $variables['attributes']['class'][] = Html::cleanCssIdentifier('folder--' . $variables['view_mode']);
  $variables['attributes']['class'][] = Html::cleanCssIdentifier('folder--' . $folder->bundle());
  $variables['attributes']['data-folders-folder-id'] = $folder->id();

  $variables['url'] = '';
  $variables['page'] = FALSE;
  $variables['teaser'] = $variables['view_mode'] === 'teaser';
  $variables['add_content_to_folder_form'] = $variables['view_mode'] === 'add_content_to_folder_form';
  if (!$folder->isNew()) {
    $variables['url'] = $folder->toUrl('canonical', ['language' => $folder->language()]);

    // See if we are rendering the folder at its canonical route.
    $route_match = \Drupal::routeMatch();
    if ($route_match->getRouteName() == 'entity.folder.canonical') {
      $page_folder = $route_match->getParameter('folder');
    }
    $is_page = !empty($page_folder) && $page_folder->id() == $folder->id();
    $variables['page'] = $variables['view_mode'] == 'full' && $is_page;
  }

  // Helpful $content variable for templates.
  $variables += ['content' => []];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Prepares variables for the folder add content link form.
 *
 * @param array $variables
 *   The variables.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function template_preprocess_folder_add_content_link(array &$variables) {
  $variables['add_content_link'] = NULL;
  $user = $variables['user'] ?? NULL;
  $entity = $variables['entity'] ?? NULL;
  if (!($user instanceof AccountInterface) || !($entity instanceof ContentEntityInterface) ) {
    return;
  }

  /** @var \Drupal\folder\FolderStorageInterface $folder_storage */
  $folder_storage = \Drupal::entityTypeManager()->getStorage('folder');
  $variables['content_folders'] = $folder_storage->loadContentFolders($entity, $user->id(), 'folder', $variables['load_content_folder_entities']);
  $variables['#cache']['tags'][] = 'folder_list:folder';
  $variables['#cache']['contexts'][] = 'user';
  $title = $variables['content_folders'] ? t('Add this content to another folder') : t('Add this content to a folder');

  $parameters = [
    'user' => $user->id(),
    'entity_type_id' => $entity->getEntityTypeId(),
    'entity_id' => $entity->id(),
  ];
  $options = [
    'query' => \Drupal::destination()->getAsArray(),
  ];
  $variables['add_content_link'] = [
    '#type' => 'link',
    '#attributes' => [
      'class' => ['use-ajax'],
      'data-dialog-options' => '{"width":"900"}',
      'data-dialog-type' => 'modal',
      'title' => $title,
    ],
    '#title' => $title,
    '#url' => Url::fromRoute('entity.folder.add_content_form', $parameters, $options),
    '#cache' => [
      'tags' => [
        $parameters['entity_type_id'] . ':' . $parameters['entity_id'],
      ],
    ],
  ];
}

/**
 * Implements hook_entity_extra_field_info().
 */
function folder_entity_extra_field_info() {
  $extra = [];
  // Add an extra "add content to folder" field to every content entity.
  $entity_types = \Drupal::entityTypeManager()->getDefinitions();
  $bundle_info = \Drupal::getContainer()->get('entity_type.bundle.info');
  $supported_entity_types = Folder::supportedEntityTypes();
  foreach ($entity_types as $entity_type_id => $entity_type) {
    if (in_array($entity_type_id, $supported_entity_types)) {
      $bundles = $bundle_info->getBundleInfo($entity_type_id);
      foreach ($bundles as $bundle => $data) {
        $extra[$entity_type_id][$bundle]['display']['folder_add_content'] = [
          'label' => t('Add content to folder link'),
          'description' => t('A link that allow to add the entity being viewed to be added to folder.'),
          'weight' => 100,
          'visible' => FALSE,
        ];
      }
    }
  }
  return $extra;
}

/**
 * Implements hook_entity_view().
 */
function folder_entity_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  $folder_add_content_component = $display->getComponent('folder_add_content');
  if ($folder_add_content_component === NULL) {
    return;
  }

  $user = \Drupal::routeMatch()->getParameter('user');
  if (!$user) {
    $user = \Drupal::currentUser();
  }
  elseif (is_numeric($user)) {
    $user = User::load($user);
  }

  $build['folder_add_content'] = [
    '#markup' => '',
  ];
  if ($user instanceof AccountInterface && $user->hasPermission('view own folder')) {
    $build['folder_add_content'] = [
      '#theme' => 'folder_add_content_link',
      '#user' => $user,
      '#entity' => $entity,
      '#load_content_folder_entities' => FALSE,
    ];
  }
  $cacheable_metadata = new CacheableMetadata();
  $cacheable_metadata->addCacheableDependency($user);
  $cacheable_metadata->applyTo($build['folder_add_content']);
}
