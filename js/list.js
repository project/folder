/**
 * @file
 * Folders list table behaviors.
 */

(function ($, Drupal) {

  /**
   * Reorder folders.
   *
   * This behavior is dependent on the tableDrag behavior, since it uses the
   * objects initialized in that behavior to update the row.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the drag behavior to an applicable table element.
   */
  Drupal.behaviors.folderDrag = {
    attach(context, settings) {
      const backStep = settings.folder.backStep;
      const forwardStep = settings.folder.forwardStep;
      // Get the folder tableDrag object.
      const tableDrag = Drupal.tableDrag.folders_list_table;
      const $table = $('#folders_list_table');
      const rows = $table.find('tr').length;

      // When a row is swapped, keep previous and next page classes set.
      tableDrag.row.prototype.onSwap = function (swappedRow) {
        $table
          .find('tr.folder-preview')
          .removeClass('folder-preview');
        $table
          .find('tr.folder-divider-top')
          .removeClass('folder-divider-top');
        $table
          .find('tr.folder-divider-bottom')
          .removeClass('folder-divider-bottom');

        const tableBody = $table[0].tBodies[0];
        if (backStep) {
          for (let n = 0; n < backStep; n++) {
            $(tableBody.rows[n]).addClass('folder-preview');
          }
          $(tableBody.rows[backStep - 1]).addClass('folder-divider-top');
          $(tableBody.rows[backStep]).addClass('folder-divider-bottom');
        }

        if (forwardStep) {
          for (let k = rows - forwardStep - 1; k < rows - 1; k++) {
            $(tableBody.rows[k]).addClass('folder-preview');
          }
          $(tableBody.rows[rows - forwardStep - 2]).addClass(
            'folder-divider-top',
          );
          $(tableBody.rows[rows - forwardStep - 1]).addClass(
            'folder-divider-bottom',
          );
        }
      };
    },
  };
})(jQuery, Drupal);
