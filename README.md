# Folder

## About

Allow users to add content entities into folders so that they can easily retrieve or organize content they are
interested in.

## Access control and permission

The module **doesn't apply any access control** on the content added into its folders. It only allows users who created
the folder to be able to organize existing content entities that they are interested into folder. Each user can only
view and manage their own created folders.

Not to be confused with files folders or directories because this module doesn't create a folder in the files system
of drupal. See the [media directories](https://www.drupal.org/project/media_directories) if you are interested with
something to organize files into directories at the site level.
